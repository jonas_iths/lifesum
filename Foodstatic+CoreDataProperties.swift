//
//  Foodstatic+CoreDataProperties.swift
//  LifeSumTest
//
//  Created by Jonas on 10/12/15.
//  Copyright © 2015 Jonas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Foodstatic {

    @NSManaged var addedbyuser: NSNumber?
    @NSManaged var brand: String?
    @NSManaged var calories: NSNumber?
    @NSManaged var carbohydrates: NSNumber?
    @NSManaged var categoryid: NSNumber?
    @NSManaged var cholesterol: NSNumber?
    @NSManaged var custom: NSNumber?
    @NSManaged var defaultsize: NSNumber?
    @NSManaged var deletedBool: NSNumber?
    @NSManaged var downloaded: NSNumber?
    @NSManaged var fat: NSNumber?
    @NSManaged var fiber: NSNumber?
    @NSManaged var gramsperserving: NSNumber?
    @NSManaged var hidden: NSNumber?
    @NSManaged var language: String?
    @NSManaged var lastupdated: NSNumber?
    @NSManaged var mlingram: NSNumber?
    @NSManaged var ocategoryid: NSNumber?
    @NSManaged var oid: NSNumber?
    @NSManaged var pcsingram: NSNumber?
    @NSManaged var pcstext: String?
    @NSManaged var potassium: NSNumber?
    @NSManaged var protein: NSNumber?
    @NSManaged var saturatedfat: NSNumber?
    @NSManaged var servingcategory: NSNumber?
    @NSManaged var showmeasurement: NSNumber?
    @NSManaged var showonlysametype: NSNumber?
    @NSManaged var sodium: NSNumber?
    @NSManaged var source_id: NSNumber?
    @NSManaged var sugar: NSNumber?
    @NSManaged var title: String?
    @NSManaged var typeofmeasurement: NSNumber?
    @NSManaged var unsaturatedfat: NSNumber?
    @NSManaged var relationshipToCategory: Categoriesstatic?

}
