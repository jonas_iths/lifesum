//
//  FoodCellTableViewCell.swift
//  LifeSumTest
//
//  Created by Jonas on 07/12/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit

class FoodCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    var entity: Foodstatic!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
