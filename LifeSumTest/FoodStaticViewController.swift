//
//  FoodStaticViewController.swift
//  LifeSumTest
//
//  Created by Jonas on 07/12/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit

class FoodStaticViewController: UITableViewController, UISearchBarDelegate  {

    var tableSourceArray: [Foodstatic] = []
    var navBarTitleForDetailViewController = ""
    var cellEntity:Foodstatic?
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewWillAppear(animated: Bool) {
        self.tabBarController?.title = "Food"


        tableSourceArray = SharedClass.sharedClass.foodStaticTableSourceArray
        self.tableView.backgroundColor = UIColor(red: 240.0/255, green: 240.0/255, blue: 229.0/255, alpha: 1.0)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60.0
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        let string = searchBar.text
        if let newtableSourceArray = SharedClass.sharedClass.getFoodStaticFromDbSearchFiltered(string!) {
            tableSourceArray = newtableSourceArray
        } else {
            tableSourceArray = []
        }
        
        // change tableview source and update
        tableView.reloadData()
        searchBar.endEditing(true)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableSourceArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! FoodCell
        
        if let title = tableSourceArray[indexPath.row].title {
            cell.title.text  = title
        }
        
        cell.entity = tableSourceArray[indexPath.row]
                
        return cell
    }

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        tableView.separatorInset = UIEdgeInsetsZero
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        let selectedCell = tableView.cellForRowAtIndexPath(indexPath)! as! FoodCell
        navBarTitleForDetailViewController = selectedCell.title.text!
        cellEntity = selectedCell.entity
        performSegueWithIdentifier("segueToFoodStaticDetail", sender: nil)

    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        
        navigationItem.backBarButtonItem = backItem
        
        let vc = segue.destinationViewController as! FoodstaticDetailViewController
        vc.navBarTitle = navBarTitleForDetailViewController
        vc.entity = cellEntity
    
    }

}
