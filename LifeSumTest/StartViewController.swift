//
//  StartViewController.swift
//  LifeSumTest
//
//  Created by Jonas on 07/12/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    var index = 0
    var shouldShowLoadingIndicator = true
    var rotation:CGFloat = 180
    
    @IBOutlet weak var loadingBackGround: UIImageView!
    @IBOutlet weak var loadingIndicator: UIImageView!
    @IBOutlet weak var exercises: UIButton!
    @IBOutlet weak var Food: UIButton!
    @IBOutlet weak var loadingTxt: UILabel!
    
    @IBAction func foodBtn(sender: AnyObject) {
        index = 0
        
        if shouldShowLoadingIndicator {
            animateInLoadingImage()
            animateLoadingImage()
        } else {
            segue()
        }
    }
    
    @IBAction func exerciseBtn(sender: AnyObject) {
        index = 1
        
        if shouldShowLoadingIndicator {
            animateInLoadingImage()
            animateLoadingImage()
        } else {
            segue()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()                
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
        // dispatch_async(dispatch_get_main_queue()) {
            
            if !SharedClass.sharedClass.checkedIfSavedToDB(2) {
                SharedClass.sharedClass.getCategoriesStaticFromJsonAndSaveToDB()
                print("Categories saved to db")
                SharedClass.sharedClass.setDBAsSaved(2)
            } else {
                print("Categories, its already saved")
            }
            
            if !SharedClass.sharedClass.checkedIfSavedToDB(1) {
                SharedClass.sharedClass.getFoodStaticFromJsonAndSaveToDB()
                print("Foodstatic, saved to db")
                SharedClass.sharedClass.setDBAsSaved(1)
            } else {
                print("Foodstatic, its already saved")
            }
            SharedClass.sharedClass.foodStaticTableSourceArray = SharedClass.sharedClass.getFoodStaticFromDb()!

            if !SharedClass.sharedClass.checkedIfSavedToDB(3) {
                SharedClass.sharedClass.getExercisesSaticFromJsonAndSaveToDB()
                print("Exercises, saved to db")
                SharedClass.sharedClass.setDBAsSaved(3)
            } else {
                print("Exercises, its already saved")
            }
            
            SharedClass.sharedClass.exercisesTableSourceArray = SharedClass.sharedClass.getExercisesStaticFromDb()!
            
            dispatch_async(dispatch_get_main_queue()) {
                // Hide loading actvity
                self.shouldShowLoadingIndicator = false
            }
            
        }
        
        // fetchrewuestcontroler
        // sortdescriptor
        // filtering with predicate
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 90.0/255, green: 209.0/255, blue: 133.0/255, alpha:    0)
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        let btnArr = [exercises, Food]
        
        for btn in btnArr {
            btn.backgroundColor = UIColor(red: 250.0/255, green: 140.0/255, blue: 40.0/255, alpha: 1.0)
            btn.layer.borderWidth = 0
            btn.layer.borderColor = UIColor.whiteColor().CGColor
            btn.layer.cornerRadius = btn.frame.size.height/2
            btn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        }
        
    }
    
    func animateInLoadingImage() {
        
        UIView.animateWithDuration (0.3, delay: 0.0, options: UIViewAnimationOptions.CurveLinear ,animations: {
            self.loadingIndicator.alpha = 1
            self.loadingTxt.alpha = 1
            self.loadingBackGround.alpha = 0.9
            }, completion: { finished in
               // No completion action
        })
        
    }
    
    func animateLoadingImage() {
        
        UIView.animateWithDuration (1.00, delay: 0.0, options: UIViewAnimationOptions.CurveLinear ,animations: {
            self.loadingIndicator.transform = CGAffineTransformMakeRotation((self.rotation * CGFloat(M_PI)) / 180.0)
            }, completion: { finished in
                
                if self.shouldShowLoadingIndicator {
                    self.rotation += 179
                    self.animateLoadingImage()
                } else {
                    // Hide loading indicator
                    self.fadeOutLoadingImage()
                    self.segue()
                }
        })
        
    }
    
    func fadeOutLoadingImage() {
        UIView.animateWithDuration (0.3, delay: 0.0, options: UIViewAnimationOptions.CurveLinear ,animations: {
                self.loadingIndicator.alpha = 0
                self.loadingTxt.alpha = 0
            }, completion: { finished in
                UIView.animateWithDuration(0.3) {
                    self.loadingBackGround.alpha = 0
                }
        })
    }
    
    func segue() {
        performSegueWithIdentifier("segueToTab", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        let backItem = UIBarButtonItem()
        backItem.title = "Back"

        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        navigationItem.backBarButtonItem = backItem
        
        let vc = segue.destinationViewController as! UITabBarController
        vc.selectedIndex = index
        
    }
    

}
