//
//  ExercisesstaticDetailViewController.swift
//  LifeSumTest
//
//  Created by Jonas on 10/12/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit

class ExercisesstaticDetailViewController: UIViewController {

    var navBarTitle:String?
    var entity:Exercisesstatic?    
    @IBOutlet weak var calories: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if let title = navBarTitle {
            self.title = title
        }
        
        if let cellEntity = entity {
            
            if let c = cellEntity.calories {
                
                calories.text = String(c)
                
            }
            
        }
        
    }

}
