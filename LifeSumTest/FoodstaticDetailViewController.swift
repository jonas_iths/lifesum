//
//  FoodstaticDetailViewController.swift
//  LifeSumTest
//
//  Created by Jonas on 10/12/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit

class FoodstaticDetailViewController: UIViewController {

    var navBarTitle:String?
    var entity:Foodstatic?
    
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var protein: UILabel!
    @IBOutlet weak var sugar: UILabel!
    @IBOutlet weak var fat: UILabel!
    @IBOutlet weak var carbohydrates: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {

        if let title = navBarTitle {
            self.title = title
        }
        
        if let cellEntity = entity {
            
            if let p = cellEntity.protein, s = cellEntity.sugar, f = cellEntity.fat, c = cellEntity.carbohydrates {
                protein.text = String(p)
                sugar.text = String(s)
                fat.text = String(f)
                carbohydrates.text = String(c)
            }
            
            if let cat = cellEntity.relationshipToCategory!.category {
                category.text = cat
            } else {
                category.text = "N/A"
            }
            
        }
        
    }

}
