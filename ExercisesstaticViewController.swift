//
//  ExercisesstaticViewController.swift
//  LifeSumTest
//
//  Created by Jonas on 10/12/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit

class ExercisesstaticViewController: UITableViewController {

    var tableSourceArray: [Exercisesstatic] = []
    var navBarTitleForDetailViewController = ""
    var cellEntity:Exercisesstatic!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60.0

    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tabBarController?.title = "Exercises"
        tableSourceArray = SharedClass.sharedClass.exercisesTableSourceArray
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        tableView.separatorInset = UIEdgeInsetsZero
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableSourceArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! ExercisesCell
        
        if let title = tableSourceArray[indexPath.row].title {
            cell.exercisesTitle.text  = title
        }
        
        cell.entity = tableSourceArray[indexPath.row]
        
        return cell
    }

    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectedCell = tableView.cellForRowAtIndexPath(indexPath)! as! ExercisesCell
        navBarTitleForDetailViewController = selectedCell.exercisesTitle.text!
        cellEntity = selectedCell.entity
        performSegueWithIdentifier("segueToExercisesStaticDetail", sender: nil)
        
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        
        navigationItem.backBarButtonItem = backItem
        
        let vc = segue.destinationViewController as! ExercisesstaticDetailViewController
        vc.navBarTitle = navBarTitleForDetailViewController
        vc.entity = cellEntity
        
    }
}
