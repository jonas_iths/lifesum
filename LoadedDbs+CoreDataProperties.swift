//
//  LoadedDbs+CoreDataProperties.swift
//  LifeSumTest
//
//  Created by Jonas on 05/12/15.
//  Copyright © 2015 Jonas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension LoadedDbs {

    @NSManaged var foodstatic: NSNumber?
    @NSManaged var categoriesstatic: NSNumber?
    @NSManaged var exercisesstatic: NSNumber?

}
