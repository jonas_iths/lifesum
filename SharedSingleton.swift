//
//  SharedSingleton.swift
//  LifeSumTest
//
//  Created by Jonas on 04/12/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class SharedClass {

    static let sharedClass = SharedClass()
    
    private init() {}
        
    var foodStaticTableSourceArray: [Foodstatic] = []
    var exercisesTableSourceArray: [Exercisesstatic] = []
    
    func checkedIfSavedToDB(whichDB:Int) -> Bool {
        // 1 == foodstatic, 2 == categories, 3 == exercises
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "LoadedDbs")
        
        do {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            let arr = results as! [LoadedDbs]
            
            if arr.count < 1 {
            
                return false
            
            } else {
                
                let entity = arr[0] 
                var boolToCheck: Bool?
                
                switch whichDB {
                case 1:
                    boolToCheck = entity.foodstatic as? Bool
                case 2:
                    boolToCheck = entity.categoriesstatic as? Bool
                case 3:
                    boolToCheck = entity.exercisesstatic as? Bool
                default:
                    print("wrong input")
                }
                
                if boolToCheck == true {
                    return true
                } else {
                    return false
                }
                
                // appDelegate.saveContext()
            
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return false
    
    }
    
    func setDBAsSaved(whichDB:Int) {
        // 1 == foodstatic, 2 == categories, 3 == exercises
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let entity =  NSEntityDescription.entityForName("LoadedDbs",
            inManagedObjectContext:managedContext)
        
        let fetchRequest = NSFetchRequest(entityName: "LoadedDbs")
        
        do {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            let arr = results as! [LoadedDbs]
            
            if arr.count < 1 {
                // create new
                let newRow = LoadedDbs(entity: entity!,
                    insertIntoManagedObjectContext: managedContext)
                
                switch whichDB {
                case 1:
                    newRow.foodstatic = true
                case 2:
                    newRow.categoriesstatic = true
                case 3:
                    newRow.exercisesstatic = true
                default:
                    print("wrong input")
                }
                
                do {
                    try managedContext.save()
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
                
            } else {
                
                // set existing
                switch whichDB {
                case 1:
                    arr[0].foodstatic = true
                case 2:
                    arr[0].categoriesstatic = true
                case 3:
                    arr[0].exercisesstatic = true
                default:
                    print("wrong input")
                }
                
                do {
                    try managedContext.save()
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
                
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    
    func getFoodStaticFromJsonAndSaveToDB() -> Bool {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let entity =  NSEntityDescription.entityForName("Foodstatic",
            inManagedObjectContext:managedContext)
        
        if let path = NSBundle.mainBundle().pathForResource("foodStatic", ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                do {
                    let jsonResult: NSArray = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                    
                    for d in jsonResult {
                        if let dict = d as? Dictionary<String, AnyObject> {

                            let newRow = Foodstatic(entity: entity!,
                                insertIntoManagedObjectContext: managedContext)
                            
                            newRow.addedbyuser = dict["addedbyuser"] as? NSNumber
                            newRow.brand = dict["brand"] as? String
                            newRow.calories = dict["calories"] as? NSNumber
                            newRow.carbohydrates = dict["carbohydrates"] as? NSNumber
                            newRow.categoryid = dict["categoryid"] as? NSNumber
                            newRow.cholesterol = dict["cholesterol"] as? NSNumber
                            newRow.custom = dict["custom"] as? NSNumber
                            newRow.defaultsize = dict["defaultsize"] as? NSNumber
                            newRow.downloaded = dict["downloaded"] as? NSNumber
                            newRow.fat = dict["fat"] as? NSNumber
                            newRow.fiber = dict["fiber"] as? NSNumber
                            newRow.gramsperserving = dict["gramsperserving"] as? NSNumber
                            newRow.hidden = dict["hidden"] as? NSNumber
                            newRow.language = dict["language"] as? String
                            newRow.lastupdated = dict["lastupdated"] as? NSNumber
                            newRow.mlingram = dict["mlingram"] as? NSNumber
                            newRow.ocategoryid = dict["ocategoryid"] as? NSNumber
                            newRow.oid = dict["oid"] as? NSNumber
                            newRow.ocategoryid = dict["ocategoryid"] as? NSNumber
                            newRow.pcsingram = dict["pcsingram"] as? NSNumber
                            newRow.pcstext = dict["pcstext"] as? String
                            newRow.potassium = dict["potassium"] as? NSNumber
                            newRow.protein = dict["protein"] as? NSNumber
                            newRow.saturatedfat = dict["saturatedfat"] as? NSNumber
                            newRow.servingcategory = dict["servingcategory"] as? NSNumber
                            newRow.showmeasurement = dict["showmeasurement"] as? NSNumber
                            newRow.showonlysametype = dict["showonlysametype"] as? NSNumber
                            newRow.sodium = dict["sodium"] as? NSNumber
                            newRow.source_id = dict["source_id"] as? NSNumber
                            newRow.sugar = dict["sugar"] as? NSNumber
                            newRow.title = dict["title"] as? String
                            newRow.typeofmeasurement = dict["typeofmeasurement"] as? NSNumber
                            newRow.unsaturatedfat = dict["unsaturatedfat"] as? NSNumber
                            newRow.deletedBool = dict["deleted"] as? NSNumber
                            
                            // The relationship to Categoriesstatic
                            newRow.relationshipToCategory = getCategory(dict["categoryid"] as! NSNumber)
                            
                        }
                    }
                    
                    do {
                        try managedContext.save()
                        // try appDelegate.saveContext()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                    
                    return true
                    
                } catch {
                    print("error jsonData")
                    return false
                }
            } catch {
                print("error jsonResult")
                return false
            }
        }
        
        return false
    
    }
    
    func getCategory(oid:NSNumber) -> Categoriesstatic? {
        
            let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
            let managedContext = appDelegate.managedObjectContext
            let fetchRequest = NSFetchRequest(entityName: "Categoriesstatic")
            let predicate = NSPredicate(format: "oid == %@", oid)
            
            // Set the predicate on the fetch request
            fetchRequest.predicate = predicate
            
            do {
                let result =
                try managedContext.executeFetchRequest(fetchRequest)
                let category = result[0] as? Categoriesstatic
                
                return category
                
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        
            return nil
        
    }
    
    func getCategoriesStaticFromJsonAndSaveToDB() -> Bool {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let entity =  NSEntityDescription.entityForName("Categoriesstatic",
            inManagedObjectContext:managedContext)
        
        if let path = NSBundle.mainBundle().pathForResource("categoriesStatic", ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                do {
                    let jsonResult: NSArray = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                    
                    for d in jsonResult {
                        if let dict = d as? Dictionary<String, AnyObject> {
                            
                            // print("category: ", d)
                            
                            let newRow = Categoriesstatic(entity: entity!,
                                insertIntoManagedObjectContext: managedContext)
                            
                            newRow.category = dict["category"] as? String
                            newRow.headcategoryid = dict["headcategoryid"] as? NSNumber
                            newRow.name_fi = dict["name_fi"] as? String
                            newRow.name_it = dict["name_it"] as? String
                            newRow.name_pt = dict["name_pt"] as? String
                            newRow.name_no = dict["name_no"] as? String
                            newRow.servingscategory = dict["servingscategory"] as? NSNumber
                            newRow.name_pl = dict["name_pl"] as? String
                            newRow.name_da = dict["name_da"] as? String
                            newRow.oid = dict["oid"] as? NSNumber
                            newRow.photo_version = dict["photo_version"] as? NSNumber
                            newRow.lastupdated = dict["lastupdated"] as? NSNumber
                            newRow.name_nl = dict["name_nl"] as? String
                            newRow.name_fr = dict["name_fr"] as? String
                            newRow.name_ru = dict["name_ru"] as? String
                            newRow.name_sv = dict["name_sv"] as? String
                            newRow.name_es = dict["name_es"] as? String
                            newRow.name_de = dict["name_it"] as? String
                            
                        }
                    }
                    
                    do {
                        try managedContext.save()
                        // try appDelegate.saveContext()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                    
                    return true
                    
                } catch {
                    print("error jsonData")
                    return false
                }
            } catch {
                print("error jsonResult")
                return false
            }
        }
        
        return false
        
    }

    func getExercisesSaticFromJsonAndSaveToDB() -> Bool {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let entity =  NSEntityDescription.entityForName("Exercisesstatic",
            inManagedObjectContext:managedContext)
        
        if let path = NSBundle.mainBundle().pathForResource("exercisesStatic", ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                do {
                    let jsonResult: NSArray = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                    
                    for d in jsonResult {
                        if let dict = d as? Dictionary<String, AnyObject> {
                            
                            // print("exercise: ", d)
                            
                            let newRow = Exercisesstatic(entity: entity!,
                                insertIntoManagedObjectContext: managedContext)
                            
                            newRow.name_pl = dict["name_pl"] as? String
                            newRow.hidden = dict["hidden"] as? Bool
                            newRow.deletedBool = dict["deleted"] as? Bool
                            newRow.downloaded = dict["downloaded"] as? Bool
                            newRow.name_da = dict["name_da"] as? String
                            newRow.photo_version = dict["photo_version"] as? NSNumber
                            newRow.custom = dict["custom"] as? Bool
                            newRow.photo_version = dict["photo_version"] as? NSNumber
                            newRow.name_pt = dict["name_pt"] as? String
                            newRow.oid = dict["oid"] as? NSNumber
                            newRow.name_no = dict["name_no"] as? String
                            newRow.name_sv = dict["name_sv"] as? String
                            newRow.name_es = dict["name_es"] as? String
                            newRow.lastupdated = dict["lastupdated"] as? NSNumber
                            newRow.name_ru = dict["name_ru"] as? String
                            newRow.addedbyuser = dict["addedbyuser"] as? Bool
                            newRow.name_de = dict["name_de"] as? String
                            newRow.title = dict["title"] as? String
                            newRow.name_fr = dict["name_fr"] as? String
                            newRow.name_nl = dict["name_nl"] as? String
                            newRow.calories = dict["calories"] as? Double
                            newRow.name_it = dict["name_it"] as? String
                            
                        }
                    }
                    
                    do {
                        try managedContext.save()
                        // try appDelegate.saveContext()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                    
                    return true
                    
                } catch {
                    print("error jsonData")
                    return false
                }
                
            } catch {
                print("error jsonResult")
                return false
            }
        }
        
        return false
        
    }
    
    func getFoodStaticFromDb() -> [Foodstatic]? {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Foodstatic")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        do {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            let arr = results as! [Foodstatic]
            
            return arr
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return nil
    }
    
    func getExercisesStaticFromDb() -> [Exercisesstatic]? {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Exercisesstatic")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        do {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            let arr = results as! [Exercisesstatic]
            
            return arr
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return nil
    }
    
    func getFoodStaticFromDbSearchFiltered(string: String) -> [Foodstatic]? {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Foodstatic")

        let predicate = NSPredicate(format: "title contains %@", string)
        
        // Set the predicate on the fetch request
        fetchRequest.predicate = predicate
    
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        do {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            let arr = results as! [Foodstatic]
            
            return arr
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return nil
    }
    
}


